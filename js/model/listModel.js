// Lấy danh sách
let renderTask = (taskList) => {
  let taskCompleted = ``;
  let taskUnCompleted = ``;
  taskList.forEach((item) => {
    let contentHTML = `
        <li>
        <span>${item.task}</span> 
        <span>
        <i onclick="handleDeleteTask('${item.id}')" class="fa fa-trash-alt" data-toggle="tooltip" data-placement="bottom" title="Delete"></i> 
        <i onclick="handleUpdateTask('${item.id}')" class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Check"></i>
        </span>
        </li>
        `;
    if (item.status == false) {
      taskUnCompleted += contentHTML;
    } else {
      taskCompleted += contentHTML;
    }
  });
  document.getElementById("todo").innerHTML = taskUnCompleted;
  if (taskCompleted == ``) {
    return;
  }
  document.getElementById("completed").innerHTML = taskCompleted;
};

// Lấy thông tin từ Form
let getInfoFromForm = () => {
  let task = document.getElementById("newTask").value;
  let status = false;
  return { task, status };
};

let toDoList = (data) => {
  let toDoList = [];
  data.forEach((item) => {
    if (item.status == false) {
      toDoList.push(item);
    }
  });
  return toDoList;
};

export { renderTask, getInfoFromForm, toDoList };
