let validator = {
  kiemTraChuoiRong: function (valueInput, idError) {
    if (valueInput.trim() == "") {
      document.getElementById(idError).innerText = "Please write task ...";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      document.getElementById(idError).style.display = "none";
      return true;
    }
  },
};
