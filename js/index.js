import { renderTask, getInfoFromForm, toDoList } from "./model/listModel.js";

let listToDo = [];
const BASE_URL = "https://6306e61dc0d0f2b80123144f.mockapi.io/";

// lấy list task
let renderTaskService = () => {
  axios({
    url: `${BASE_URL}/list`,
    method: "GET",
  })
    .then((res) => {
      renderTask(res.data);
      listToDo = toDoList(res.data);
      console.log("res.data: ", res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
renderTaskService();

//add task
let handleAddTask = () => {
  let dataTask = getInfoFromForm();

  let isValid = validator.kiemTraChuoiRong(dataTask.task, "tb");
  if (isValid == false) {
    return;
  }
  axios({
    url: `${BASE_URL}/list`,
    method: "POST",
    data: dataTask,
  })
    .then((res) => {
      console.log("res: ", res);
      renderTaskService();
      document.getElementById("newTask").value = "";
      location.reload();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.handleAddTask = handleAddTask;

//update task
let handleUpdateTask = (id) => {
  axios({
    url: `${BASE_URL}/list/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res.data);
      changeStatus(res.data, id);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.handleUpdateTask = handleUpdateTask;

let changeStatus = (data, id) => {
  let dataTask = data;
  if (dataTask.status == false) {
    dataTask.status = true;
  } else {
    dataTask.status = false;
  }

  axios({
    url: `${BASE_URL}/list/${id}`,
    method: "PUT",
    data: dataTask,
  })
    .then((res) => {
      console.log("res: ", res);
      renderTaskService();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

//delete task
let handleDeleteTask = (id) => {
  axios({
    url: `${BASE_URL}/list/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log("res: ", res);
      renderTaskService();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

window.handleDeleteTask = handleDeleteTask;

// Sắp xếp tăng dần
let xepTangDan = () => {
  listToDo.sort((a, b) => {
    if (a.task.toLowerCase() < b.task.toLowerCase()) {
      return -1;
    }
    return 0;
  });
  renderTask(listToDo);
};

window.xepTangDan = xepTangDan;

// Sắp xếp giảm dần
let xepGiamDan = () => {
  listToDo.sort((a, b) => {
    if (a.task.toLowerCase() > b.task.toLowerCase()) {
      return -1;
    }
    return 0;
  });
  renderTask(listToDo);
};

window.xepGiamDan = xepGiamDan;
